import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';

const { Header } = Layout;

function PageHeader() {
  return (
    <Header className="header" style={{ background: '#fff', padding: 0, paddingLeft: 16 }}>
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">
          Home
          <Link to="/" />
        </Menu.Item>

        <Menu.Item key="2">
          About
          <Link to="/about" />
        </Menu.Item>
      </Menu>
    </Header>
  );
}

export default PageHeader;
