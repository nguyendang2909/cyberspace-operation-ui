import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Layout } from 'antd';
import 'antd/dist/antd.css';

import PageHeader from './layouts/page-header';
import PageFooter from './layouts/page-footer';
import PageSider from './layouts/page-sider';
import PageBody from './layouts/page-body';

class RouterApp extends Component {
  render() {
    return (
      <Router>
        <Layout style={{ minHeight: '100vh' }}>
          <PageSider />
          <Layout>
            <PageHeader />
            <PageBody />
            <PageFooter />
          </Layout>
        </Layout>
      </Router>
    );
  }
}

export default RouterApp;
